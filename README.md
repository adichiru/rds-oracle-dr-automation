# Disaster Recovery (DR) solution for AWS RDS Oracle
RDS Oracle does not have an active Multi-Region solution; you can't have the equivalent of read-replicas in a secondary region that is updated in real time and can be used actively for read queries like MySQL and PostgreSQL engines have in RDS. Due to this, there is also no easy/built-in DR solution for an RDS Oracle DB instance.

To solve this, we need to piece together a few things to create backups, move them to the DR region and restore them, automatically and frequently while optimizing the cost as much as possible.

The followings have been loosely based on this AWS blog post:
https://aws.amazon.com/blogs/database/cross-region-automatic-disaster-recovery-on-amazon-rds-for-oracle-database-using-db-snapshots-and-aws-lambda/

Some of the code in those examples does not work properly and some steps are missing. Also, the post is considered a PoC, so we needed to adjust a few things, including a more secure IAM role/policy.

## Goals

- [x] Automatically take a snapshot of Oracle RDS instance every 1 day at 3AM PST, in the primary region (us-east1).
- [x] Automatically copy this snapshot to the secondary region (us-west2).
- [x] Automatically clean up older snapshots (keep only the last 7 snapshots).
- [ ] Automatically restore the latest snapshot into the RDS service in the secondary region when there is an availability error in the primary region.
This functionality is not yet automated (it must be triggered manually for now).

## Requirements
- IAM role with proper permissions
- RTO/RPO requirements

## Logic
- CloudWatch Events Rule based on date/time -> triggers Lambda function to initiate an RDS snapshot
- RDS event subscription for **backup** publishes to SNS topic -> triggers Lambda function when snapshot is complete to copy it to the secondary region
- RDS event subscription for **availability error** publishes to SNS topic -> triggers Lambda function to restore the latest snapshot in the secondary region
- CloudWatch Events Rule based on date/time -> triggers Lambda function to delete snapshots older than *n* days

*Note: There should not be too many snapshots kept available because there is a limit at the RDS level of 100 manual snapshots ([more details on RDS limits](https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/CHAP_Limits.html#RDS_Limits.Limits)).*

## Implementation

**In the primary region:**

1. Create the IAM role so that Lambda functions can operate on RDS
   - In the AWS Console, under Services find IAM.
   - Go to Roles -> Create role -> AWS Service -> Lambda
     - Permissions – create a new policy:
       - use the contents of the **Allow-Lambda-DR-Oracle-RDS.json** file.
       - Name: **Allow-Lambda-DR-Oracle-RDS**
       - Description: *This policy allows Lambda to work with RDS and snapshots in order to automate the DR for RDS Oracle.*
     - Role name: **RDS-Oracle-DR-via-Lambda-Role**
     - Role description: *Allows Lambda functions to work with RDS and snapshots to automate the DR for Oracle.*

*Note: this policy gives permissions to RDS on both regions.*

2. Create the RDS event subscriptions
   - In the AWS Console, under Services find RDS.
   - Go to Event Subscriptions -> Create event subscription
      - Name: **Prod-Oracle-DB-backup-initiated-event-subscription**
      - ARN: **Prod_Oracle_DB_backup_initiated_primary_region**
      - Source type:
        - instances -> select specific instance: [instance name]
        - Event categories -> select specific event categories: **backup**

   - Go to Event Subscriptions -> Create event subscription
      - Name: **Prod-Oracle-DB-availability_error-event-subscription**
      - ARN: **Prod_Oracle_DB_availability_error_primary_region**
      - Source type:
        - instances -> select specific instance: [instance name]
        - Event categories -> select specific event categories: **availability**

3. Create the SNS topics
   - In the AWS Console, under Services find Simple Notification Service (SNS).
   - Create topic:
     - Topic name: **Prod_Oracle_DB_backup_initiated_primary_region**
     - Display Name: not necessary
   - Create topic:
     - Topic name: **Prod_Oracle_DB_availability_error_primary_region**
     - Display name: not necessary

4. Create the Lambda function to take snapshots
   - In the AWS Console, under Services find Lambda.
   - Create function:
     - Author from Scratch
     - Name: **RDS-Oracle-DR-backup-snapshot-creation**
     - Runtime: Python2.7
     - Role -> chose existing role: **RDS-Oracle-DR-via-Lambda-Role**
     - Function code -> use the contents of the **RDS-Oracle-DR-backup-snapshot-creation.py** file.
     - Basic settings -> Timeout: 1 min

5. Create the Lambda function to copy the snapshots
   - In the AWS Console, under Services find Lambda.
   - Create function:
     - Author from Scratch
     - Name: **RDS-Oracle-DR-backup-snapshot-copy-to-secondary-region**
     - Runtime: Python2.7
     - Role -> chose existing role: **RDS-Oracle-DR-via-Lambda-Role**
     - Function code -> use the contents of the **RDS-Oracle-DR-backup-snapshot-copy-to-secondary-region.py** file.
     - Basic settings -> Timeout: 1 min

6. Create the Lambda function to delete old snapshots
   - In the AWS Console, under Services find Lambda.
   - Create function:
     - Author from Scratch
     - Name: **RDS-Oracle-DR-backup-snapshot-deletion**
     - Runtime: Python2.7
     - Role -> chose existing role: **RDS-Oracle-DR-via-Lambda-Role**
     - Function code -> use the contents of the **RDS-Oracle-DR-backup-snapshot-deletion.py** file.
     - Basic settings -> Timeout: 1 min

7. Create the CloudWatch Events Rule to trigger the snapshots creation
   - In the AWS Console, under Services find CloudWatch.
   - Rules -> Create rule:
     - Schedule -> Cron expression: 0 11 * * ? *
     - Targets: Lambda function: **RDS-Oracle-DR-backup-snapshot-creation**
     - Name: **Create-RDS-Oracle-backup-snapshot-prod**
     - Description: *This rule is triggering a snapshot for the RDS Oracle DB instance for backup/DR purposes. This is not the built-in recurrent backup of RDS.*
     - State: Enabled

8. Create the CloudWatch Events Rule to trigger the old snapshots deletion
   - In the AWS Console, under Services find CloudWatch.
   - Rules -> Create rule:
     - Schedule -> Cron expression: 0 13 * * ? *
     - Targets: Lambda function: **RDS-Oracle-DR-backup-snapshot-deletion**
     - Name: **Delete-RDS-Oracle-backup-snapshot-prod**
     - Description: *This rule is triggering a Lambda function to delete older RDS Oracle DB snapshots.*
     - State: Enabled


**In the secondary region:**

9. Create the Lambda function to restore the snapshots
   - In the AWS Console, under Services find Lambda.
   - Create function:
     - Author from Scratch
     - Name: **RDS-Oracle-DR-backup-snapshot-restoration**
     - Runtime: Python2.7
     - Role -> chose existing role: **RDS-Oracle-DR-via-Lambda-Role**
     - Function code -> use the contents of the **RDS-Oracle-DR-backup-snapshot-restoration.py** file.
     - Basic settings -> Timeout: 1 min


## Testing Lambda functions

To test the above functions simply run a test (Test button) with no parameters. This functionality sometimes is not very stable; I've seen error reports after a previous error that were not real, so use with care and patience and refresh everything if needed.
