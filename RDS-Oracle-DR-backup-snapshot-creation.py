#!/usr/bin/env python

# python2.7 lambda script

# To do:
# - extract parameters/variables from python scripts to Lambda tags

import botocore
import datetime
import re
import logging
import boto3

# Variables:
# Main/primary AWS region (e.g. us-west2)
primary_region = ''
# A list with the instances to handle
instances = ['']


def lambda_handler(event, context):
    source = boto3.client('rds', region_name=primary_region)
    for instance in instances:
        print "Instance is {0}".format(instance)
        try:
            timestamp = str(datetime.datetime.now().strftime('%Y-%m-%d-%H-%-M-%S'))
            snapshot = "{0}-{1}-{2}".format("lambda-oracle-backup-snapshot-for-DR", instance, timestamp)
            response = source.create_db_snapshot(DBSnapshotIdentifier=snapshot, DBInstanceIdentifier=instance)
            print(response)
        except botocore.exceptions.ClientError as e:
            raise Exception("Could not create backup snapshot: %s" % e)
