#!/usr/bin/env python

# python2.7 lambda script

# To do:
# - extract parameters/variables from python scripts to Lambda tags

import boto3
import botocore
import datetime
import re
import logging

# Variables:
# Main/primary AWS region (e.g. us-west2)
primary_region = ''
# DR/secondary AWS region
secondary_region = ''
# DB instance class (e.g. db.t2.large)
db_instance_class = ''
# The subnet to be used in the seconday region
db_subnet = ''
# A list with the instances to handle
instances = ['']

print('Loading function')

def byTimestamp(snap):
  if 'SnapshotCreateTime' in snap:
    return datetime.datetime.isoformat(snap['SnapshotCreateTime'])
  else:
    return datetime.datetime.isoformat(datetime.datetime.now())

def lambda_handler(event, context):
    source = boto3.client('rds', region_name=secondary_region)
    for instance in instances:
        try:
            source_snaps = source.describe_db_snapshots(DBInstanceIdentifier = instance)['DBSnapshots']
            print "DB_Snapshots:", source_snaps
            source_snap = sorted(source_snaps, key=byTimestamp, reverse=True)[0]['DBSnapshotIdentifier']
            snap_id = (re.sub( '-\d\d-\d\d-\d\d\d\d ?', '', source_snap))
            print('Will restore %s.' % (source_snap))
            response = source.restore_db_instance_from_db_snapshot(DBInstanceIdentifier=instance,DBSnapshotIdentifier=source_snap,DBInstanceClass=db_instance_class,DBSubnetGroupName=db_subnet,MultiAZ=False,PubliclyAccessible=False)
            print(response)

        except botocore.exceptions.ClientError as e:
            raise Exception("Could not restore: %s" % e)
