#!/usr/bin/env python

# python2.7 lambda script

# To do:
# - extract parameters/variables from python scripts to Lambda tags

import boto3
import botocore
import datetime
import re
import json

# Variables:
# Main/primary AWS region (e.g. us-west2)
primary_region = ''
# A list with the instances to handle
instances = ['']


iam_client = boto3.client('iam')

def byTimestamp(snap):
    if 'SnapshotCreateTime' in snap:
        return datetime.datetime.isoformat(snap['SnapshotCreateTime'])
    else:
        return datetime.datetime.isoformat(datetime.datetime.now())

def lambda_handler(event, context):
    print("Received event: " + json.dumps(event, indent=2))
    account_ids = []
    try:
        iam_client.get_user()
    except Exception as e:
        account_ids.append(re.search(r'(arn:aws:sts::)([0-9]+)', str(e)).groups()[1])
        account = account_ids[0]

    source = boto3.client('rds', region_name=primary_region)

    for instance in instances:
        source_instances = source.describe_db_instances(DBInstanceIdentifier=instance)
        source_snaps = source.describe_db_snapshots(DBInstanceIdentifier=instance)['DBSnapshots']
        source_snap = sorted(source_snaps, key=byTimestamp, reverse=True)[0]['DBSnapshotIdentifier']
        source_snap_arn = 'arn:aws:rds:%s:%s:snapshot:%s' % (primary_region, account, source_snap)
        target_snap_id = (re.sub('rds:', '', source_snap))
        print('Copying %s to %s.' % (source_snap_arn, target_snap_id))
        target = boto3.client('rds', region_name=secondary_region)

        try:
            response = target.copy_db_snapshot(
                SourceDBSnapshotIdentifier=source_snap_arn,
                TargetDBSnapshotIdentifier=target_snap_id,
                CopyTags = True)
            print(response)
        except botocore.exceptions.ClientError as e:
            raise Exception("Could not issue copy command: %s" % e)
        copied_snaps = target.describe_db_snapshots(SnapshotType='manual', DBInstanceIdentifier=instance)['DBSnapshots']
