#!/usr/bin/env python

# python2.7 lambda script

# To do:
# - extract parameters/variables from python scripts to Lambda tags

import json
import boto3
from datetime import datetime, timedelta, tzinfo

# Variables:
# Main/primary AWS region (e.g. us-west2)
primary_region = ''
# retention period for backup snapshots (in days):
retention_period = 7



class Zone(tzinfo):
    def __init__(self,offset,isdst,name):
        self.offset = offset
        self.isdst = isdst
        self.name = name
    def utcoffset(self, dt):
        return timedelta(hours=self.offset) + self.dst(dt)
    def dst(self, dt):
        return timedelta(hours=1) if self.isdst else timedelta(0)
    def tzname(self,dt):
        return self.name

UTC = Zone(10,False,'UTC')

retentionDate = datetime.now(UTC) - timedelta(days=retention_period)
#retentionDate = datetime.now(UTC)

def lambda_handler(event, context):
    print("Connecting to RDS")
    rds = boto3.setup_default_session(region_name=primary_region)
    client = boto3.client('rds')
    snapshots = client.describe_db_snapshots(SnapshotType='manual',DBInstanceIdentifier='master')

    print('Deleting all DB Snapshots older than %s.' % retentionDate)

    for i in snapshots['DBSnapshots']:
        if i['SnapshotCreateTime'] < retentionDate:
            print ('Deleting snapshot %s...' % i['DBSnapshotIdentifier'])
            client.delete_db_snapshot(DBSnapshotIdentifier=i['DBSnapshotIdentifier'])
